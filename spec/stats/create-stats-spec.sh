#!/usr/bin/env bash

for file
do
	# script=${file%[0-9]*}
	script=${file%/*}
	path=$LKP_SRC/programs/$script/parse
	[ -x $path ] || path=$LKP_SRC/stats/$script
	echo \
	"$path < $file > ${file}.yaml"
	$path $file < $file > ${file}.yaml
done
