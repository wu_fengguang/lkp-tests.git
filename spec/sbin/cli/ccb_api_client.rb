require 'json'

require "#{File.dirname(File.dirname(File.realpath($PROGRAM_NAME)))}/cli/ccb_api_client"

class CcbApiClient
  def initialize(host = '172.17.0.1', port = 10_012)
    @host = host
    @port = port
    @url_prefix = url_prefix
  end

  def update_os_project(jwt, os_project, request_json)
    url = "#{@url_prefix}#{@host}:#{@port}/api/os/#{os_project}/snapshot"
    return { 'status_code' => 200, 'url' => url }.to_json
  end

  def search(jwt, request_json)
    url = "#{@url_prefix}#{@host}:#{@port}/data-api/search"
    if request_json.include? 'rpms'
      return {'status_code' => 200, 'url' => url, 'hits' => {
        'hits' => ['_source' => {'rpm_repo' => 'test', 'rpms_detail' => {'a' => 'b'}}]}}.to_json
    elsif request_json.include? 'result_root'
      return {"hits" => {"hits" => ["_source" => {"result_root" => "/result/test/cbs.1"}]}}.to_json
    elsif request_json.include? 'test-cancel-build-id'
      return {"hits" => {"hits" => ["_source" => {"os_project" => "cancel"}]}}.to_json
    end
    return {'status_code' => 200, 'url' => url, 'hits' => {'hits' => {'build_id' => 'test'}}}.to_json
  end

  def create_branch_project(jwt, os_project, sub_project, request_json)
    url = "#{@url_prefix}#{@host}:#{@port}/api/os/#{os_project}/sub-project/#{sub_project}"
    return { 'status_code' => 200, 'url' => url }.to_json
  end

  def create_os_project(jwt, os_project, request_json)
    url = "#{@url_prefix}#{@host}:#{@port}/api/os/#{os_project}"
    return { 'status_code' => 200, 'url' => url }.to_json
  end

  def create_snapshot(jwt, os_project, request_json)
    url = "#{@url_prefix}#{@host}:#{@port}/api/os/#{os_project}/snapshot"
    return { 'status_code' => 200, 'url' => url }.to_json
  end

  def abort_build(jwt, os_project, request_json)
    url = "#{@url_prefix}#{@host}:#{@port}/api/os/#{os_project}/abort_build"
    return { 'status_code' => 200, 'url' => url }.to_json
  end

  def build_single(jwt, os_project, request_json)
    url = "#{@url_prefix}#{@host}:#{@port}/api/os/#{os_project}/build_single"
    return { 'status_code' => 200, 'url' => url }.to_json
  end

  def build_dag(jwt, os_project, request_json)
    url = "#{@url_prefix}#{@host}:#{@port}/api/os/#{os_project}/build_dag"
    return { 'status_code' => 200, 'url' => url }.to_json
  end
end
