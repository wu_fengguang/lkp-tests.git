#! /usr/bin/env ruby

LKP_SRC = ENV['LKP_SRC'] || File.dirname(File.dirname(File.realpath($PROGRAM_NAME)))

should_get_str = (
'{
  "status_code": 200,
  "url": "http://172.17.0.1:111/api/os/openEuler:Mainline/build_dag"
}
')

describe 'build' do
  it 'test_build' do
    output = `LKP_SRC=#{LKP_SRC}spec ruby #{LKP_SRC}/sbin/cli/build os_project=openEuler:Mainline build_type=full`
    expect(output).to eq should_get_str
  end
end
