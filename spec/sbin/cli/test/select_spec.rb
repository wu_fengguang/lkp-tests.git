#! /usr/bin/env ruby

LKP_SRC = ENV['LKP_SRC'] || File.dirname(File.dirname(File.realpath($PROGRAM_NAME)))

should_get_str = (
'{
  "build_id": "test"
}
')

describe 'select' do
  it 'test_select' do
    output = `LKP_SRC=#{LKP_SRC}spec ruby #{LKP_SRC}/sbin/cli/select builds build_id=test`
    expect(output).to eq should_get_str
  end
end
