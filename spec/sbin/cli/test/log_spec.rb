#! /usr/bin/env ruby

LKP_SRC = ENV['LKP_SRC'] || File.dirname(File.dirname(File.realpath($PROGRAM_NAME)))

describe 'log' do
  it 'test_log' do
    output = `LKP_SRC=#{LKP_SRC}spec ruby #{LKP_SRC}/sbin/cli/log cbs.1`
    expect(output).to eq "http://172.17.0.1:333/result/test/cbs.1\n"
  end
end
