#! /usr/bin/env ruby

LKP_SRC = ENV['LKP_SRC'] || File.dirname(File.dirname(File.realpath($PROGRAM_NAME)))

should_get_str = (
'{
  "status_code": 200,
  "url": "http://172.17.0.1:111/api/os/openEuler:Mainline/build_single"
}
')

describe 'build_single' do
  it 'test_build_single' do
    output = `LKP_SRC=#{LKP_SRC}spec ruby #{LKP_SRC}/sbin/cli/build-single os_project=openEuler:Mainline packages=gcc`
    expect(output).to eq should_get_str
  end
end
