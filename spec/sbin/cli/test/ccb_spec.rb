#! /usr/bin/env ruby

LKP_SRC = ENV['LKP_SRC'] || File.dirname(File.dirname(File.realpath($PROGRAM_NAME)))

describe 'ccb' do
  it 'test_ccb_rb' do
    output = `ruby #{LKP_SRC}/sbin/cli/ccb.rb --help`
    expect(output.include?('Usage: ccb [global options] sub_command [sub_command options] [args]')).to eq true
  end

  it 'test_ccb' do
    output = `#{LKP_SRC}/sbin/cli/ccb.rb --help`
    expect(output.include?('Usage: ccb [global options] sub_command [sub_command options] [args]')).to eq true
  end

  it 'test_ccb_rb select' do
    output = `ruby #{LKP_SRC}/sbin/cli/ccb.rb select`
    expect(output.include?('Usage: ccb select <index> k=v|--json JSON|--yaml YAML --sort --field')).to eq true
  end

  it 'test_ccb select' do
    output = `#{LKP_SRC}/sbin/cli/ccb.rb select`
    expect(output.include?('Usage: ccb select <index> k=v|--json JSON|--yaml YAML --sort --field')).to eq true
  end
end
