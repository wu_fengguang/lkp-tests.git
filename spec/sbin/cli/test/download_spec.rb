#! /usr/bin/env ruby

LKP_SRC = ENV['LKP_SRC'] || File.dirname(File.dirname(File.realpath($PROGRAM_NAME)))

describe 'download' do
  it 'test_download' do
    `LKP_SRC=#{LKP_SRC}spec ruby #{LKP_SRC}/sbin/cli/download os_project=openEuler:Mainline packages=gcc architecture=aarch64 -b all -s -d`
    dir = "#{LKP_SRC}spec/sbin/cli/openEuler:Mainline-aarch64-gcc"
    file = dir + '/a'
    expect(File.exists?(file)).to eq true
    File.delete(file)
    Dir.delete(dir)
  end
end
