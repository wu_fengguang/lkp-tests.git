#! /usr/bin/env ruby

LKP_SRC = ENV['LKP_SRC'] || File.dirname(File.dirname(File.realpath($PROGRAM_NAME)))

create_project = (
'{
  "status_code": 200,
  "url": "http://172.17.0.1:111/api/os/openEuler:Mainline"
}
')

create_branch_project = (
'{
  "status_code": 200,
  "url": "http://172.17.0.1:111/api/os/true/sub-project/openEuler:Mainline"
}
')

create_snapshot = (
'{
  "status_code": 200,
  "url": "http://172.17.0.1:111/api/os/openEuler:Mainline/snapshot"
}
'
)

describe 'create' do
  it 'test_create_branch_project' do
    output = `LKP_SRC=#{LKP_SRC}spec ruby #{LKP_SRC}/sbin/cli/create projects openEuler:Mainline inherited_from=true project=test`
    expect(output).to eq create_branch_project
  end

  it 'test_create_os_project' do
    output = `LKP_SRC=#{LKP_SRC}spec ruby #{LKP_SRC}/sbin/cli/create projects openEuler:Mainline project=test`
    expect(output).to eq create_project
  end

  it 'test_create_snapshot' do
    output = `LKP_SRC=#{LKP_SRC}spec ruby #{LKP_SRC}/sbin/cli/create snapshots openEuler:Mainline snapshot=test`
    expect(output).to eq create_snapshot
  end
end
