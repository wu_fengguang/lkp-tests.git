#! /usr/bin/env ruby

LKP_SRC = ENV['LKP_SRC'] || File.dirname(File.dirname(File.realpath($PROGRAM_NAME)))

should_get_str = (
'{
  "status_code": 200,
  "url": "http://172.17.0.1:111/api/os/test/snapshot"
}
')

describe 'update' do
  it 'test_update' do
    output = `LKP_SRC=#{LKP_SRC}spec ruby #{LKP_SRC}/sbin/cli/update projects test -j #{LKP_SRC}spec/sbin/cli/update.json`
    expect(output).to eq should_get_str
  end
end
