#!/usr/bin/env ruby

require "#{File.dirname(File.dirname(File.realpath($PROGRAM_NAME)))}/cli/ccb_common"

def load_jwt?(force_update=false)
  return true
end

def load_my_config
  return {
    'GATEWAY_IP' => '172.17.0.1',
    'GATEWAY_PORT' => '111',
    'SRV_HTTP_REPOSITORIES_PROTOCOL' => 'http://',
    'SRV_HTTP_REPOSITORIES_HOST' => '172.17.0.1',
    'SRV_HTTP_REPOSITORIES_PORT' => '222',
    'SRV_HTTP_RESULT_PROTOCOL' => 'http://',
    'SRV_HTTP_RESULT_HOST' => '172.17.0.1',
    'SRV_HTTP_RESULT_PORT' => '333'
  }
end
