# Use text mode install
text


%packages
@^minimal-environment

%end

reboot --eject

# System language
lang en_US.UTF-8

# Network information
network  --hostname=localhost.localdomain

# Use hard drive installation media
harddrive --dir= --partition=LABEL=openEuler-21.09-aarch64

# Run the Setup Agent on first boot
firstboot --enable
# Do not configure the X Window System
skipx
# System services
services --enabled="chronyd"

ignoredisk --only-use=vda
# System bootloader configuration
bootloader --location=mbr --boot-drive=vda
autopart
# Partition clearing information
clearpart --all --initlabel --drives=vda

# System timezone
timezone Asia/Shanghai --utc

# Root password
rootpw --iscrypted $6$/qKHpGryfTqHMoo/$JCMcgxFqienzLbHxYiB0dcmczGKr.Mv0gZVoFxlsz1mUuBJXyEaFxDXk79qNhB92cqNgEC6rHL92w7QVxpTiq.

%addon com_redhat_kdump --disable --reserve-mb='128'

%end

%anaconda
pwpolicy root --minlen=8 --minquality=1 --strict --nochanges --notempty
pwpolicy user --minlen=8 --minquality=1 --strict --nochanges --emptyok
pwpolicy luks --minlen=8 --minquality=1 --strict --nochanges --notempty
%end
