# Generated by Anaconda 36.16.5
# Generated by pykickstart v3.47
#version=DEVEL
# Use graphical install
graphical

# Keyboard layouts
keyboard --vckeymap=us --xlayouts='us'
# System language
lang en_US.UTF-8

# Use hard drive installation media
harddrive --dir= --partition=LABEL=openEuler-22.03-LTS-SP3-x86_64

%packages
@^minimal-environment

%end

# Run the Setup Agent on first boot
firstboot --enable

# Generated using Blivet version 3.4.2
ignoredisk --only-use=vda
# System bootloader configuration
bootloader --location=mbr --boot-drive=vda
autopart
# Partition clearing information
clearpart --all --initlabel --drives=vda

# System timezone
timezone Asia/Shanghai --utc

# Root password
rootpw --iscrypted $y$j9T$HfUXqdkMPJ5wnK1DzdaK7jwi$zZ.SZC7aAaz9nbMwPfWuHnEPRc/IBv316jU4QXMybg6

%post
sed -i -e 's/SELINUX=enforcing/SELINUX=permissive/g' /etc/selinux/configuration
%end

# Reboot after installation
reboot --eject