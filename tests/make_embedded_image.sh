#!/bin/bash
# repo_url
# branch "openEuler-22.09"
# upload_image_dir

. $LKP_SRC/lib/upload.sh
set -e

function make_embedded_img()
{
	if [ ! -n "${branch}" ];then
		echo "[ERROR] branch is empty"
		exit 1
	fi
	
	arch="$(uname -m)"
	if [ "${arch}" != "x86_64" ];then
		echo "[ERROR] can not support architecture: ${arch}"
		exit 1
	fi
	
	yum install -y git java tar cmake gperf sqlite-devel libffi-devel \
		xz-devel zlib zlib-devel openssl-devel bzip2-devel ncurses-devel \
		readline-devel libpcap-devel parted autoconf-archive chrpath gcc-c++ \
		patch rpm-build flex autoconf automake m4 bison bc libtool gettext-devel \
		createrepo_c git net-tools wget sudo hostname rpcgen texinfo python meson \
		dosfstools mtools libmpc-devel gmp-devel ninja-build numactl-devel make \
		gcc python3-pip glibc-all-langpacks glibc-locale-archive
	
	pip3 install Gitpython --trusted-host https://repo.huaweicloud.com -i https://repo.huaweicloud.com/repository/pypi/simple
	tools_dir="/usr1/tools"
	gcc_install_dir="/usr1/openeuler/gcc"
	rm -rf ${tools_dir} && mkdir -p ${tools_dir}
	rm -rf ${gcc_install_dir} && mkdir -p ${gcc_install_dir}

	cd ${tools_dir}
	wget https://repo.huaweicloud.com/openeuler/openEuler-22.03-LTS/EPOL/main/x86_64/Packages/gcc-cross-1.0-0.oe2203.x86_64.rpm -P ${tools_dir}
	rpm2cpio ${tools_dir}/gcc-cross-1.0-0.oe2203.x86_64.rpm | cpio -id
	
	cd ${gcc_install_dir}
	tar -xf ${tools_dir}/tmp/openeuler_gcc_arm32le.tar.gz
	find ./openeuler_gcc_arm32le -type d | xargs chmod go+x
	chmod -R go+r ./openeuler_gcc_arm32le
	chmod -R 755 /usr1/openeuler/gcc/openeuler_gcc_arm32le/bin
	
	tar -xf ${tools_dir}/tmp/openeuler_gcc_arm64le.tar.gz
	find ./openeuler_gcc_arm64le -type d | xargs chmod go+x
	chmod -R go+r ./openeuler_gcc_arm64le
	chmod -R 755 /usr1/openeuler/gcc/openeuler_gcc_arm64le/bin
	find ./ | xargs chmod go+x
	rm -rf ${tools_dir}
	
	rm -rf /etc/localtime
	ln -s /usr/share/zoneinfo/Asia/Shanghai /etc/localtime
	date
	
	src_path="/usr1/openeuler/src"
	result_dir="/usr1/output"
	src_tar_gz="/tmp/src.tar.gz"
	rm -rf ${src_path} ${result_dir}
	mkdir -p ${src_path}
	
	if [ -f "${src_tar_gz}" ];then
		tar zxf "${src_tar_gz}" -C /usr1/openeuler/
		rm -rf ${src_path}/yocto-meta-openeuler
	fi
	
	rm -rf /tmp/openeuler-os-build
	git clone https://gitee.com/openeuler/openeuler-os-build.git -b master /tmp/openeuler-os-build
	if [ $? -ne 0 ];then
		echo "[ERROR] clone openeuler-os-build failed"
		exit 1
	fi
	build_script="/tmp/openeuler-os-build/script/embedded/compile/build.sh"
	if [ "${branch}" != "openEuler-22.03-LTS" ];then
		sed -i 's/sh \"${SRC_DIR}\"\/yocto-meta-openeuler\/scripts\/compile.sh\"dsoftbus\"/echo \"need build dsoftbus"/g' ${build_script}
	fi
	
	if [ "${branch}" == "master" ];then
		bash -x ${build_script} "arm-std aarch64-std raspberrypi4-64" yes yes no "" "${branch}" ""
	else
		bash -x ${build_script} "arm-std aarch64-std" yes yes no "openeuler-image" "${branch}" "${branch}"
	fi
	rm -rf /tmp/openeuler-os-build
	ls ${result_dir}/$(date +%Y)*
	dest_dir="${upload_image_dir}/embedded_img/"
	upload_one_curl ${result_dir}/$(date +%Y)*/* ${dest_dir}
}

make_embedded_img
