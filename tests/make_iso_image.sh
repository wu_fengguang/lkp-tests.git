#!/bin/bash
# image_type "iso/docker/raspi/stratovirt/virtual_machine/embedded"
# iso_type "standard/everything/everything_src/everything_debug/edge/desktop"
# product "openEuler"
# version "22.09"
# release ""
# repo_url "repo url address"
# upload_image_dir "/repositories/release-images/${task_id}/"

set -ex
. $LKP_SRC/lib/upload.sh

function config_xml()
{
	normal_xml="/opt/oemaker/config/${ARCH}/normal.xml"
	rpmlist_xml="/opt/oemaker/config/rpmlist.xml"
	temp_xml="/opt/oemaker/config/${ARCH}/temp.xml"
	rpm_sh="/opt/oemaker/rpm.sh"
	grub_pkg=(grub2-pc grub2-pc-modules)
	head -n5 ${normal_xml} > ${temp_xml}
	xmllint --xpath "/comps/group[id='core']" ${normal_xml} >> ${temp_xml}
	pkgs=$(xmllint --xpath "//packagelist[@type='${ARCH}']/node()" ${rpmlist_xml} | grep packagereq | cut -d ">" -f 2 | cut -d "<" -f 1)
	packages+=(${pkgs[@]})
	if [ "${ARCH}" == "x86_64" ];then
		packages+=(${grub_pkg[@]})
	fi
	for rpmname in ${packages[@]}
	do
		sed -i '/<packagelist>/a\      <packagereq type="mandatory">'$rpmname'</packagereq>' ${temp_xml}
	done
	xmllint --xpath "/comps/group[id='anaconda-tools']" ${normal_xml} >> ${temp_xml}
	xmllint --xpath "/comps/environment[id='minimal-environment']" ${normal_xml} >> ${temp_xml}
	echo "</comps>" >> ${temp_xml}
	rm -f ${rpmlist_xml} ${normal_xml}
	mv ${temp_xml} ${normal_xml}
	sed -i '/parse_rpmlist_xml "${ARCH}"/,+3d' ${rpm_sh}
	sed -i '/^    parse_rpmlist_xml/d' ${rpm_sh}
}

function make_iso_image()
{
	ARCH="$(uname -m)"
	if rpm -q oemaker &> /dev/null; then
		yum remove oemaker -y
	fi
	if rpm -q lorax &> /dev/null; then
		yum remove lorax -y
	fi
	
	if [ "${iso_type}" == "edge" ];then
		yum install oemaker lorax linux-firmware -y -c "${yum_conf}"
	else
		yum install oemaker lorax -y -c "${yum_conf}"
	fi
	if [ "${image_type}" == "iso_normal" ];then
		config_xml
	fi
	if [ "${iso_type}" == "everything_src" ];then
		sed -i '/parse_rpmlist_xml \"conflict\"/d' "/opt/oemaker/rpm.sh"
		dest_dir="${upload_image_dir}/ISO/source/"
	elif [ "${iso_type}" == "desktop" ];then
		dest_dir="${upload_image_dir}/workstation/${ARCH}/"
	elif [ "${iso_type}" == "edge" ];then
		dest_dir="${upload_image_dir}/edge_img/${ARCH}/"
	else
		dest_dir="${upload_image_dir}/ISO/${ARCH}/"
	fi

	cd /opt/oemaker
	set +e
	num=0
	set +u
	while [ "${num}" -lt 3 ]
	do
		bash -x oemaker -t ${iso_type} -p ${product} -v "${version}" -r "${release}" -s "${repo_url}"
		if [ $? -eq 0 ];then
			break
		elif [ $? -eq 133 ]; then
			sleep 60
			((num=num+1))
			continue
		else
			echo "[ERROR] make ${iso_type} iso fail"
			exit 1
		fi
	done
	if [ "${num}" -ge 3 ]; then
		echo "[ERROR] retry make ${iso_type} iso fail"
		exit 1
	fi
	set -e
	cd "/result"
	ISO_NAME=$(ls *-dvd.iso)
	if [ x"${ISO_NAME}" == x'' ];then
		echo "[ERROR] can not find iso"
		exit 1
	fi
	sha256sum "${ISO_NAME}" > "${ISO_NAME}.sha256sum"
	upload_one_curl "${ISO_NAME}" "${dest_dir}"
	upload_one_curl "${ISO_NAME}.sha256sum" "${dest_dir}"
	need_rpmlist_iso=(standard edge desktop)
	if [[ "${need_rpmlist_iso[@]}" =~ "${iso_type}" ]];then
		if [ "${iso_type}" == "standard" ];then
			if [ -n "${release}" ];then
				iso_rpmlist="${product}-${version}-${release}-${ARCH}.rpmlist"
			else
				iso_rpmlist="${product}-${version}-${ARCH}.rpmlist"
			fi
		fi
		if [ "${iso_type}" == "edge" ];then
			if [ -n "${release}" ];then
				iso_rpmlist="${product}-${version}-${release}-edge-${ARCH}.rpmlist"
			else
				iso_rpmlist="${product}-${version}-edge-${ARCH}.rpmlist"
			fi
		fi
		if [ "${iso_type}" == "desktop" ];then
			if [ -n "${release}" ];then
				iso_rpmlist="${product}-Desktop-${version}-${release}-${ARCH}.rpmlist"
			else
				iso_rpmlist="${product}-Desktop-${version}-${ARCH}.rpmlist"
			fi
		fi
		rm -rf temp && mkdir temp
		mount *-dvd.iso temp
		ls temp/Packages/*.rpm > "${iso_rpmlist}"
		umount temp
		[ -n temp ] && rm -rf temp
		upload_one_curl "${iso_rpmlist}" "${dest_dir}"
	fi
	echo "[INFO] make ${iso_type} iso success"
}

make_iso_image
