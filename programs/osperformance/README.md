# OSPerformance工具使用方法

> 适用于uos-deb和uos-rpm系，root账户运行

### 1.下载lkp-test

```
git clone https://toscode.gitee.com/compass-ci/lkp-tests.git
```

### 2.安装lkp-test

```
cd lkp-tests && make install
lkp install
```

### 3.安装osperformance

```
cd programs/osperformance
lkp split jobs/osperformance.yaml
lkp install osperformance-stream.yaml  ## stream可替换为fio/lmbench等其他工具，如果osperformance有更新，需要加-f参数安装
```

> 安装后的代码路径为：/lkp/benchmark/OSPerformance

### 4.运行osperformance

```
lkp run osperformance-stream.yaml  ## stream可替换为fio/lmbench等其他工具
```

### 5.查看结果

```
1./lkp/benchmark/OSPformance/report   #ospformance工具结果
2./lkp-test/programs/osperformance/result   #lkp-test工具结果(包含osperformance结果)
```

