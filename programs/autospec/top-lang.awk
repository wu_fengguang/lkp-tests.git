BEGIN { 
    var=ENVIRON["LKP_SRC"]"/programs/autospec/language_map.yaml"
    # Load language mapping from YAML file
    while (getline < var) {
	split($0, parts, ":")
	language = parts[1]
	split(parts[2], suffixes, " ")
	for (i in suffixes) {
	    language_map[suffixes[i]] = language
	}
    }
    close(var)
}
{
    suffix = $NF
    if (suffix in language_map) {
	suffix_counts[suffix]++
    }
}
END {
    for (suffix in suffix_counts) {
	lang_counts[language_map[suffix]] += suffix_counts[suffix]
    }
    for (lang in lang_counts) {
	print lang_counts[lang], lang
    }
}
