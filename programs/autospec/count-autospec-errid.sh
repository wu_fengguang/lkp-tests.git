#!/bin/bash
# count errid with latest code/pattern, most useful for quick evaluation of new patterns

group_id=$1
rr=$(cci jobs -f result_root group_id=$group_id)

export JOB_PP_KEYS=autospec
unset RESULT_ROOT
shopt -s nullglob

for dir in $rr
do
	[ -f $dir/stderr ] || continue
	cat $dir/stderr $dir/stdout $dir/*/build.log |
		grep -if $LKP_SRC/etc/stderr-pattern |
		$LKP_SRC/stats/stderr |
		grep ': 1$'
done | sort | uniq -c | sort -nr
