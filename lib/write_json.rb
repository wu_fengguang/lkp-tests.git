#!/usr/bin/env ruby

require 'json'

TMP_RESULT_ROOT = ENV["TMP_RESULT_ROOT"] || "/tmp/lkp/result"

def write_json(content, filename)
  unless File.directory?(TMP_RESULT_ROOT)
    Dir.mkdir(TMP_RESULT_ROOT)
  end
  filepath = File.join TMP_RESULT_ROOT, filename
  File.open(filepath, 'w') do |f|
    f.write(JSON.pretty_generate(content))
  end
end
