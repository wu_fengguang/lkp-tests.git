#!/bin/sh

tbox_cant_kexec()
{
	is_virt && return 0

	has_cmd kexec || return 0
	has_cmd cpio || return 0

	return 1
}

set_tbox_group()
{
	local tbox=$1

	if echo "$tbox" | grep -Eq '^(.*)-[0-9]+$'; then
		local tbox_rematch=$(echo $tbox | sed -r 's|(.*)-[0-9]+|\1 |')
		tbox_group=$(echo $tbox_rematch | sed -r 's#-[0-9]+-#-#')
	else
		tbox_group=$tbox
	fi
}

create_host_config() {
	[ -n "$DRY_RUN" ] && return

	local host_config="$LKP_SRC/hosts/${HOSTNAME}"
	[ -e $host_config ] || {
		echo "Creating testbox configuration file: $host_config"

		local mem_kb="$(grep MemTotal /proc/meminfo | awk '{print $2}')"
		local mem_gb="$(((mem_kb)/1024/1024))"
		local nr_cpu=$(nproc)
		local hdd_yaml=
		local ssd_yaml=

		for hdd in ${hdd_partitions}
		do
			hdd_yaml="${hdd_yaml}\n- ${hdd}"
		done

		for ssd in ${ssd_partitions}
		do
			ssd_yaml="${ssd_yaml}\n- ${ssd}"
		done

		mkdir -p $LKP_SRC/hosts
		cat <<EOT >> $host_config
nr_cpu: $nr_cpu
memory: ${mem_gb}G
hdd_partitions:
$(printf "$hdd_yaml")
ssd_partitions:
$(printf "$ssd_yaml")
local_run: 1
EOT
	}

	local tbox_group
	set_tbox_group $HOSTNAME

	local host_group_config="$LKP_SRC/hosts/${tbox_group}"
	[ -e $host_group_config ] || {
		echo "Creating testbox group configuration file: $host_group_config"
		cp $host_config $host_group_config
	}
}

