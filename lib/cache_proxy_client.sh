#! /bin/bash
download_source()
{
	local sources=()

	local spec_sources=$(spectool -S  "$1" | awk -F ': ' '{print $2}')
	for source in $spec_sources; do
		if [[ $source == http:* ]]; then
			sources+="$source"
		fi
		if [[ $source == https:* ]]; then
			sources+="$source"
		fi
	done

	local proxy="https://cache-proxy.test.osinfra.cn/download"
	for source in "${sources[@]}"; do
		local obs_source=$proxy/$source
		wget $obs_source -P $2
		local base_name=`basename $source`
		if [ ! -f "$2/$base_name" ]; then
			echo "Failed to download from cache-proxy.Start downloading from the official website."
			wget $source -P $2
		fi
	done
}
