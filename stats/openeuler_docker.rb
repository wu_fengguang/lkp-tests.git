#!/usr/bin/env ruby

LKP_SRC = ENV['LKP_SRC'] || File.dirname(File.dirname(File.realpath($PROGRAM_NAME)))
require "#{LKP_SRC}/lib/tests/stats"

def pre_handle(stats, file_path)
  status = false
  repo_set = Set[]
  sys_set = Set[]

  File.readlines(file_path).each do |line|
    case line.chomp!

    # Error: Unable to find a match: docker-registry mock xx
    when /Error: Unable to find a match: (.+)/
      $1.split.each do |repo|
        repo_set << repo
      end

    # RUN yum -y swap -- remove fakesystemd -- install systemd systemd-libs
    # yum swap: error: unrecognized arguments: install systemd systemd-libs
    when /yum swap: error: .*: install (.+)/
      $1.split.each do |sys|
        sys_set << sys
      end

    # curl: (22) The requested URL returned error: 404 Not Found
    # error: skipping https://dl.fedoraproject.org/pub/epel/bash-latest-7.noarch.rpm - transfer failed
    when /.*error: .* (https.*)/
      stats.add_test_error line, 'requested-URL-returned.error'
      status = true

    # Error: Unknown repo: 'powertools'
    when /Error: Unknown repo: (.+)/
      repo = $1.delete!("'")
      stats.add_test_error line, "unknown-repo.#{repo}"
      status = true

    # Error: Module or Group 'convert' does not exist.
    when /Error: Module or Group ('[^\s]+')/
      repo = $1.delete!("'")
      stats.add_test_error line, "error.not-exist-module-or-group.#{repo}"
      status = true

    # /bin/sh: passwd: command not found
    when /\/bin\/sh: (.+): command not found/
      stats.add_test_error line, "sh.command-not-found.#{$1}"
      status = true
    end

    repo_set.each do |repo|
      stats.add_test_error line, "yum.error.Unable-to-find-a-match.#{repo}"
      status = true
    end

    sys_set.each do |sys|
      stats.add_test_error line, "yum.swap.error.unrecognized-arguments-install.#{sys}"
      status = true
    end
  end
  status
end

def handle_unknown_error(file_path)
  line_num = %x(cat #{file_path} | grep -n 'Step '  | tail -1 | awk -F: '{print $1}')

  index = 1
  message = ''
  File.readlines(file_path).each do |line|
    if index == Integer(line_num)
      message += line
    else
      index += 1
    end
  end

  message = $1 if message =~ %r(\u001b\[91m(.+))
  message
end

def openeuler_docker(log_lines)
  stats = LKP::Stats.new

  log_lines.each do |line|
    next unless line =~ %r(([^\s]+).(build|run)\.fail)

    key, value = line.split(':')
    key.chomp!
    stats.add_id key, value.to_i

    file_path = "#{RESULT_ROOT}/#{$1}" # $1 named by docker-image name
    next unless File.exist?(file_path)
    next if pre_handle(stats, file_path)

    stats.add_id "msg.#{key}"], handle_unknown_error(file_path)
  end

  stats.result
end
