#!/usr/bin/env bash
# SPDX-License-Identifier: MulanPSL-2.0+
# Copyright (c) 2022 Huawei Technologies Co., Ltd. All rights reserved.
. $LKP_SRC/lib/email.sh

update_file()
{
	local result_file=$1
	local testbox=$3
	local testcase=$4

	[ -n "$2" ] && lab_dir="lab-$2"
	[ -n "$5" ] && SEND_MAIL_HOST=$5

	cd /c
	[ -d "${lab_dir}" ] || git clone https://gitee.com/compass-ci/"${lab_dir}".git
	[ -n "${testcase}" ] && {
		if [ "${testcase}" == "host-info" ];then
			mkdir -p "/c/${lab_dir}/hosts"
			hardware_dir="/c/${lab_dir}/hosts"
			vim -c '/arch/,/nr_cpu/m 0' -c 'wq' "${result_file}"
		elif [ "${testcase}" == "boards-scan" ];then
			mkdir -p "/c/${lab_dir}/devices"
			hardware_dir="/c/${lab_dir}/devices"
			sed -i "1,2d" "${result_file}"
		fi
	}

	[ -f "${hardware_dir}/${testbox}" ] && diff_content=$(diff "${result_file}" "${hardware_dir}/${testbox}")

	if [ "${diff_content}" != "" ] || [ ! -f "${hardware_dir}/${testbox}" ];then
		cd ${hardware_dir}
		git config --global user.name "all"
		git config --global user.email "all@crystal.ci"
		cp "${result_file}" "${testbox}"
		git add "${testbox}"
		git commit -s -m "update ${testbox}-${testcase}"
	else
		exit
	fi
}

get_recipient_email()
{
	recipient_email_to="all@crystal.ci"
}

get_report_content()
{
	patch=$(git format-patch HEAD^ --subject-prefix="PATCH ${lab_dir}")
	sed -i "1d" "${patch}"
	report_content=$(cat $patch)
}

send_mail()
{
	get_report_content
	get_recipient_email

	local_send_email_encode patch_info
}

update_file "$@"
send_mail
