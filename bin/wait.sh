#!/usr/bin/env sh

[ -n "$LKP_SRC" ] || export LKP_SRC=$(dirname $(dirname $(readlink -e -v $0)))

. $LKP_SRC/bin/event/wait.sh

"$@"
