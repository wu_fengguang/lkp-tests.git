#!/usr/bin/env bash

[ -n "$TMP" ] || TMP=/tmp

# NOTICE: should cleanup $EVENT_PIPE_DIR between jobs
# otherwise the next job's wait commands wont be blocked
# 	rm -f $TMP/event_pipe/* 
# in LKP, $TMP will be recreated in refresh_lkp_tmp()
EVENT_PIPE_DIR=$TMP/event_pipe

create_pipe()
{
	local pipe_name=$1

	test -e  $EVENT_PIPE_DIR/$pipe_name && return

	mkdir -p $EVENT_PIPE_DIR
	mkfifo   $EVENT_PIPE_DIR/$pipe_name
}

lkp_wait()
{
	local timeout=
	local pipe_name=$1
	create_pipe $pipe_name

	if [ -n "$BASH_VERSION" ]; then
		[ "$2" = '--timeout' ] && timeout="-t $3"

		read $timeout -n 1 < $EVENT_PIPE_DIR/$pipe_name
	else
		[ "$2" = '--timeout' ] && timeout="timeout $3"
		$timeout sh -c ": < $EVENT_PIPE_DIR/$pipe_name"
	fi
}

lkp_wakeup()
{
	local efd
	local pipe_name=$1
	create_pipe $pipe_name

	if [ -n "$BASH_VERSION" ]; then
		# make sure the next seq line wont be blocked
		#
		# https://stackoverflow.com/questions/41603787/how-to-find-next-available-file-descriptor-in-bash
		# Feature available since bash 4.1+ (2009-12-31): {varname} style automatic file descriptor allocation
		exec {efd}<> $EVENT_PIPE_DIR/$pipe_name
	else
		exec 9<> $EVENT_PIPE_DIR/$pipe_name
	fi

	# this writes near 4096 bytes (1 page), so support waking up near 4096
	# wait clients, each client will consume 1 byte in wakeup()
	seq -w 999 > $EVENT_PIPE_DIR/$pipe_name
}

# pre-create to avoid "file already exist" warnings due to race conditions when
# calling concurrently from various monitor scripts
create_pipe pre-test
create_pipe activate-monitor
create_pipe post-test
create_pipe job-finished
